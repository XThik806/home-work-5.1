import PropTypes from "prop-types"

function Button({ type, classNames, onClick = () => {}, children }) {

    return (
        <button className={classNames} onClick={onClick} type={type}>{children}</button>
    )
}


Button.propTypes = {
    color: PropTypes.string, 
    onClick: PropTypes.func, 
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
}

export default Button