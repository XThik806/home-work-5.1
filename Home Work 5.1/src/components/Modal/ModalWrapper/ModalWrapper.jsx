import React from 'react';
import PropTypes from 'prop-types';

const ModalWrapper = ({children, onClick = () => {}}) => {

    return (
        <>
            <div className='modalWrapper' onClick={onClick}>{children}</div>
        </>
    );
}

// ModalWrapper.propTypes = {
//     children: PropTypes.oneOfType([
//         PropType.
//     ]).isRequired,
// }

export default ModalWrapper;
