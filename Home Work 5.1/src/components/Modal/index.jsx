import PropTypes from "prop-types";

const ModalWrapper = ({ children, onClick = () => {}}) => {

    return (

        <div className="modal" onClick={onClick}>
            <div className="background" onClick={onClick}>{children}</div>
        </div>

    )
}


ModalWrapper.propTypes = {
    children: PropTypes.node,

}
export default ModalWrapper;
