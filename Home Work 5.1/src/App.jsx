import { useState } from 'react';
import "./modal.scss";
import "./button.scss";
import Button from './components/Button'
import Modal from './components/Modal'
import ModalWrapper from './components/ModalWrapper';
import ModalHeader from './components/ModalHeader';
import ModalFooter from "./components/ModalFooter";
import ModalClose from "./components/ModalClose";
import ModalBody from "./components/ModalBody";


function App() {
  const imageUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRC0czagZhuMjy2VUZnZQQopSvaiguo6CDE-FXi0AxybFhvTtu6Za4T1x4huw&s";

  const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
  const [isSecondModalOpen, setIsSecondModalOpen] = useState(false);



  return (
    <>
      <div className='wrapper'>
        <Button onClick={() => { setIsFirstModalOpen(true) }}>Open first modal</Button>
        <Button onClick={() => { setIsSecondModalOpen(true) }}>Open second modal</Button>
      </div>


      {isFirstModalOpen && (
        <Modal onClick={() => { setIsFirstModalOpen(false)}} children={<div className='background' onClick={() => { setIsFirstModalOpen(false)}}></div>}>
          <ModalWrapper>
          <ModalClose onClick={() => { }}></ModalClose>
          <ModalHeader children={<img className='modal__img' src={imageUrl} alt="image" />} />
          <ModalBody>
            <h2 className='header'>Product Delete!</h2>
            <p className='paragraph'>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</p>
          </ModalBody>
          <ModalFooter firstText="NO,CANCEL"
            secondaryText='YES,DELETE'
            firstClick={() => { }}
            secondaryClick={() => { }} />
          </ModalWrapper>

        </Modal>
      )}


      {isSecondModalOpen && (
        <Modal children={<div className='background' onClick={() => { setIsSecondModalOpen(false) }}></div>

        }>
          <ModalWrapper>
            <ModalClose onClick={() => { }}></ModalClose>
            <ModalHeader children={<h3 className='header'>Add Product “NAME”</h3>} />
            <ModalBody>
              <p className='paragraph'>Description for you product</p>
            </ModalBody>
            <ModalFooter
              firstText={"ADD TO FAVORITE"}
              firstClick={() => { }} />
          </ModalWrapper>

        </Modal>
      )}

    </>
  )
}

export default App
